def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t fomar123/my-rep .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push fomar123/my-rep'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this
